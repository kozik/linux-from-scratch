# Linux from scratch

## Toolchain

```
git clone https://github.com/crosstool-ng/crosstool-ng
cd crosstool-ng/
git checkout crosstool-ng-1.24.0
```

```
./bootstrap
./configure --enable-local
make
./ct-ng help
```

```
./ct-ng armv6-rpi-linux-gnueabi
./ct-ng menuconfig
./ct-ng build.4
export PATH="${PATH}:`pwd`/.build/armv6-rpi-linux-gnueabi/buildtools/bin/"
```

## Bootloader

```
cd ..
git clone git://git.denx.de/u-boot.git
cd u-boot/
git checkout v2019.04
```

```
make CROSS_COMPILE=armv6-rpi-linux-gnueabi- rpi_0_w_defconfig
make CROSS_COMPILE=armv6-rpi-linux-gnueabi-
```

## SD card

```
sudo dd if=/dev/zero of=/dev/X bs=4096
sudo fdisk /dev/X
```

```
mkfs.fat -F 32 -I /dev/Xp1
mkfs.ext4 /dev/Xp2
```

```
mkdir sd_1
sudo mount /dev/Xp1 sd_1
cd sd_1/
sudo wget https://github.com/raspberrypi/firmware/raw/07937c7d48bcd44cc1015c6257ae2cfa5da51298/boot/bootcode.bin
sudo wget https://github.com/raspberrypi/firmware/raw/07937c7d48bcd44cc1015c6257ae2cfa5da51298/boot/start.elf
sudo cp ../u-boot/u-boot.bin ./
sudo sed -i -e "s/BOOT_UART=0/BOOT_UART=1/" bootcode.bin
sudo sh -c "echo 'kernel=u-boot.bin' > config.txt"
cd ..
sync
sudo umount sd_1
```

## Serial

```
sudo screen /dev/ttyUSB0 115200-8-N-1
```

## Kernel

```
git clone https://github.com/torvalds/linux.git
cd linux
git checkout v5.1
```

```
make ARCH=arm bcm2835_defconfig
make menuconfig
make -j 4 ARCH=arm CROSS_COMPILE=armv6-rpi-linux-gnueabi- zImage
make ARCH=arm dtbs
```

```
sudo mount /dev/Xp1 ../sd_1
sudo cp arch/arm/boot/zImage ../sd_1/
sudo cp arch/arm/boot/dts/bcm2835-rpi-zero-w.dtb ../sd_1/
sudo umount ../sd_1
```

In das U-Boot:
```
fatload mmc 0:1 0x02100000 zImage
fatload mmc 0:1 0x02f00000 bcm2835-rpi-zero-w.dtb
bootz 0x02100000 - 0x02f00000
```

## File system

```
mkdir myinit
cd myinit
armv6-rpi-linux-gnueabi-gcc -static myinit.c -o myinit
find . | cpio -H newc -ov --owner root:root > ../myinitramfs.cpio
cd ..
gzip myinitramfs.cpio
mkimage -A arm -O linux -T ramdisk -d myinitramfs.cpio.gz myRamdisk
sudo cp myRamdisk sd_1/
```

In das U-Boot:
```
fatload mmc 0:1 0x02100000 zImage
fatload mmc 0:1 0x02f00000 bcm2835-rpi-zero-w.dtb
fatload mmc 0:1 0x03000000 myRamdisk
setenv bootargs rdinit=/myinit root=/dev/ram0
bootz 0x02100000 0x03000000 0x02f00000
```

## BusyBox

```
mkdir rootfs
cd rootfs

mkdir bin dev etc home lib proc sbin sys tmp usr var
cd usr/
mkdir bin lib sbin
cd ../var
mkdir log
```

```
cd ../..
git clone git://busybox.net/busybox.git
cd busybox/
git checkout 1_30_stable
make distclean
make defconfig
make menuconfig
make -j 4 ARCH=arm CROSS_COMPILE=armv6-rpi-linux-gnueabi- install
mkdir etc/init.d
```

```
sudo chmod +x etc/init.d/rcS
```

```
mkdir sd_2
sudo mount /dev/Xp2 sd_2
sudo cp -r rootfs/* sd_2
sudo chown -R root:root sd_2/*
sudo umount sd_2
```

In das U-Boot:
```
fatload mmc 0:1 0x02100000 zImage
fatload mmc 0:1 0x02f00000 bcm2835-rpi-zero-w.dtb
setenv bootargs root=/dev/mmcblk0p2
bootz 0x02100000 - 0x02f00000
```

## Bootenv

```
cd u-boot
make CROSS_COMPILE=armv6-rpi-linux-gnueabi-
cd ..
sudo mount /dev/mmcblk0p1 sd_1
sudo cp u-boot/u-boot.bin sd_1/
sudo umount sd_1
```

In das U-boot:
```
printenv bootcmd
```
